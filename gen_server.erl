-module(gen_server_template).

-behavior(gen_server).
-export([start_link/0, init/1]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-include("$zx_include/zx_logger.hrl").

-record(s,
        {}).


%%% startup

start_link() ->
    gen_server:start_link(?MODULE, none, []).


init(none) ->
    State = #s{},
    ok = tell(info, "t_game:init, State=~p", [State]),
    {ok, State}.


%%% message handling

handle_call(Unexpected, From, State) ->
    ok = tell(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


handle_cast(stop, State) ->
    ok = tell(info, "Received a 'stop' message."),
    {stop, normal, State};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


handle_info(Unexpected, State) ->
    ok = tell(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


%%% private fns
